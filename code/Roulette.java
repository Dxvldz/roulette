import java.util.Scanner;

public class Roulette {
    public static void main(String[]args) {
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int money = 1000;
        int betAmount = 0;
        int betNumber = 0;
        String betAnswer ="";
        String quitGame = "";


            // Would you like to bet
            do {
                System.out.println("Would you like to bet? : Yes or No");
                betAnswer = scan.nextLine();

                if (!(betAnswer.equalsIgnoreCase("yes") || betAnswer.equalsIgnoreCase("no"))) {
                    System.out.println("Please try again.");
                }
            } while (!(betAnswer.equalsIgnoreCase("yes") || betAnswer.equalsIgnoreCase("no")));

        // big do while loop to restart game
        do {
            // if yes how much
            boolean isNumber = true;
            if(betAnswer.equalsIgnoreCase("yes")) {
                do {
                
                    String amountInput;
                    do{
                        System.out.println("How much would you like to bet?");
                        amountInput = scan.nextLine();
                    }  while (!isNumber(amountInput));
                    betAmount = Integer.parseInt(amountInput);
                    if (betAmount> money) {
                        System.out.println("You don't have enough funds please try again.");
                    }
                } while (betAmount > money);

                    do {
                        String betInput;
                        do{
                        System.out.println("What number would you like to bet on : (0 to 36)");
                        betInput = scan.nextLine();
                        }  while (!isNumber(betInput));
                        betNumber = Integer.parseInt(betInput);
                        if (betNumber > 36 || betNumber < 0) {
                        System.out.println("You cannot bet those numbers. Please Try again.");
                        }    
                    } while (betNumber > 36 || betNumber < 0);

                // Spin wheel
                wheel.spin();
                boolean winLoss = isWinner(betNumber, wheel);
                if (winLoss==true) {
                    System.out.println("Your number was : " + betNumber);
                    System.out.println("The number was : " + wheel.getValue());
                    int amountWon = betAmount*35;
                    System.out.println("You've Won " + amountWon + "$!");
                    money = money+amountWon;
                    System.out.println("Your total balance is : " + money + "$");
                }
                else {
                    System.out.println("Your number was : " + betNumber);
                    System.out.println("The number was : " + wheel.getValue());
                    System.out.println("You've lost " + betAmount + "$ :(");
                    money = money - betAmount;
                    System.out.println("Your total balance is : " + money + "$");
                }

                do {
                System.out.println("Would you like to replay : Yes or No");
                quitGame = scan.nextLine();

                if (!(quitGame.equalsIgnoreCase("yes") || quitGame.equalsIgnoreCase("no"))) {
                    System.out.println("I don't quite understand. Please try again.");
                }
                betAmount=0;
                betNumber=0;
                } while (!(quitGame.equalsIgnoreCase("yes") || quitGame.equalsIgnoreCase("no")));
            }  

            if (betAnswer.equalsIgnoreCase("no")) {
                break;
            }
        } while (!(quitGame.equalsIgnoreCase("no")));

        System.out.println("Your final total is : " + money +"$");
        if (money > 1000) {
            System.out.println("You've won " + (money - 1000) + "$ today!");
        }
        else if (money < 1000) {
             System.out.println("You've lost " + (1000 - money) + "$ today...");
        }
        System.out.println("Have a good day.");
    }

    // check if String is numeric
    public static boolean isNumber(String num){
        try {
          int number = Integer.parseInt(num);
          return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    //method to determine if he win
    public static boolean isWinner(int userInput, RouletteWheel wheel) {
        if (wheel.getValue() == userInput) {
            return true;
        }
        else {
            return false;
        }
    }


}
