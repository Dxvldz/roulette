import java.util.Random;

public class RouletteWheel {
   // Setting private fields 
    private Random rand; 
    private int lastSpin;
    
    // Constructor
    public RouletteWheel() {
        this.rand = new Random();
        this.lastSpin = 0;
    }

    //Method used to update to a new random integer
    public void spin() {
        this.lastSpin = rand.nextInt(37);
    }

    // return the random value
    public int getValue() {
        return this.lastSpin;
    }
}
